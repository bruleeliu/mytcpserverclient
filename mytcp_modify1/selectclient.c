#define _WIN32
#ifdef __linux__
#undef _WIN32
#endif

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
using namespace std;

#ifdef _WIN32
#include <WinSock2.h>
#include <Windows.h>
#include <WS2tcpip.h>
#define close(x) closesocket(x)
#endif

#ifdef __linux__
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <wait.h>
#endif

#define STDIN 0
#define MYPORT "9044"
#define MAXDATASIZE 1024 // max size for each send
int sockfd; // client socket

void close_app(int s) {
	/* close app */
	// To Do...
	fprintf(stdout, "\nclient: app is closing....(%d)\n", s);
	close(sockfd);
	exit(0);
}

/* get IPv4 or IPv6 from sockaddr */
void* get_in_addr(struct sockaddr* sa)
{
	if (sa->sa_family == AF_INET) 
	{
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}
	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int TcpClientSetup(const char* hostip)
{
	/* variable */
	struct addrinfo hints;
	struct addrinfo* hostinfo;
	struct addrinfo* p; // point for for-loop
	int status;
	char hostIP[INET6_ADDRSTRLEN];


	/* get hostinfo */
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC; // IPv4 or IPv6
	hints.ai_socktype = SOCK_STREAM;
	status = getaddrinfo(hostip, MYPORT, &hints, &hostinfo);
	if (status != 0)
	{
		fprintf(stderr, "client: getaddrinfo error - %s\n", gai_strerror(status));
		return 1;
	}

	/* create socket() at first could be bind in hostinfo */
	for (p = hostinfo; p != NULL; p = p->ai_next)
	{
		sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		if (sockfd == -1)
		{
			//perror("server: socket fail and try next");
			fprintf(stderr, "client: create socket error: %s(errno: %d), try next\n", strerror(errno), errno);
			continue;
		}

		/* create connect() */
		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1)
		{
			//perror("server: bind");
			fprintf(stderr, "client: bind socket error: %s(errno: %d), try next\n", strerror(errno), errno);
			close(sockfd);
			continue;
		}
		break; // create socket pass and bind pass
	}

	if (p == NULL)
	{
		fprintf(stderr, "client: failed to bind\n");
		return 2;
	}

	/* list host IP you connected */
	inet_ntop(p->ai_family, get_in_addr((struct sockaddr*)(p->ai_addr)), hostIP, sizeof(hostIP));
	fprintf(stdout, "client: connect to host %s\n", hostIP);

	freeaddrinfo(hostinfo); // free hostinfo here (not be used below)

	return 0;
}

int main(int argc, char* argv[])
{
	/* variable */
	int status;
	int numberbytes;
	char buffer[MAXDATASIZE];
	fd_set myfdsetlist;
	int fdmax;
	fd_set temp_fds;
	struct timeval tv;


	/* check input */
	if (argc != 2)
	{
		cerr << "Usage: ./myclient hostIP" << endl;
		return 0;
	}

	/* setup client, create socket, connect.. */
	if ((status = TcpClientSetup(argv[1])) != 0)
	{
		fprintf(stderr, "client: initialize failed\n");
		exit(status);
	}

	/* register close server with ctrl+C */
	signal(SIGINT, close_app);
	fdmax = STDIN;


	/* initialize variables */
	FD_ZERO(&myfdsetlist);
	memset(buffer, 0, sizeof(buffer));

	/* set variables */
	FD_SET(STDIN, &myfdsetlist);
	FD_SET(sockfd, &myfdsetlist);
	if (sockfd > fdmax)
		fdmax = sockfd; // set max number


	/* main loop */
	while (1)
	{
		temp_fds = myfdsetlist;

		tv.tv_sec = 60;
		tv.tv_usec = 0;

		status = select(fdmax + 1, &temp_fds, NULL, NULL, &tv);
		if (status == -1)
		{
			fprintf(stderr, "client: select() failed\n");
			close(sockfd);
			exit(4);
		}
		else if (status == 0)
		{
			fprintf(stdout, "[No any messages, waiting...]\n");
			continue;
		}
		else
		{
			/* handle message */
			if (FD_ISSET(STDIN, &temp_fds))
			{
				fgets(buffer, sizeof(buffer), stdin);
				fprintf(stdout, ":<= %s\n", buffer);
				send(sockfd, buffer, strlen(buffer), 0); // send 
				memset(buffer, 0, sizeof(buffer));
			}
			if (FD_ISSET(sockfd, &temp_fds))
			{
				numberbytes = recv(sockfd, buffer, sizeof(buffer), 0);
				fprintf(stdout, ":=> %s\n", buffer);
				memset(buffer, 0, sizeof(buffer));
			}
		}
	}

	/* close socket */
	close(sockfd);

	return 0;
}