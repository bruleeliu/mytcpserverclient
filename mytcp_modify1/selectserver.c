#define _WIN32
#ifdef __linux__
#undef _WIN32
#endif

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <vector>
using namespace std;

#ifdef _WIN32
#include <WinSock2.h>
#include <Windows.h>
#include <WS2tcpip.h>
#define close(x) closesocket(x)
#endif

#ifdef __linux__
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <wait.h>
#include <sys/time.h>
#endif


#define MYPORT "9044"
#define BACKOG 10	// max amount for waiting for connecting queue
#define MAXDATASIZE 1024 // max size for each send
int listener;		// host listens socket descriptor
std::vector<int> sockfd_list; // master file descriptor list

int AssignFdSetList(fd_set *fdset)
{
	FD_ZERO(fdset);

	for(int i=0; i< (int)sockfd_list.size(); i++)
	{
		FD_SET(sockfd_list[i], fdset);
	}

	return 0;
}

void close_app(int s) {
	/* close server */
	// To Do...
	fprintf(stdout, "\nserver: app is closing....(%d)\n", s);
	close(listener);
	exit(0);
}

#ifdef __linux__
void sigchld_handler(int s)
{
	while (waitpid(-1, NULL, WNOHANG) > 0);
}

int KillZombieProcess()
{
	struct sigaction sa; // for linux kill zombie process

	/* clear all linux zombie process*/
	sa.sa_handler = sigchld_handler; // kill all zombie process
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;

	if (sigaction(SIGCHLD, &sa, NULL) == -1)
	{
		return 1;
	}
	return 0;
}
#endif

/* get IPv4 or IPv6 from sockaddr */
void* get_in_addr(struct sockaddr* sa)
{
	if (sa->sa_family == AF_INET) 
	{
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}
	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}


int TcpHostSetup()
{
	/* variable */
	int status;
	int yes = 1; // for setsockopt() to configure SO_REUSEADDR
	struct addrinfo hints;
	struct addrinfo* hostinfo;
	struct addrinfo* p;


	/* get hostinfo */
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	status = getaddrinfo(NULL, MYPORT, &hints, &hostinfo);
	if (status != 0)
	{
		fprintf(stderr, "server: getaddrinfo error - %s\n", gai_strerror(status));
		return 1;
	}

	/* create socket() at first could be bind in hostinfo */
	for (p = hostinfo; p != NULL; p = p->ai_next)
	{
		listener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		if (listener == -1)
		{
			fprintf(stderr, "server: create socket error: %s(errno: %d), try next\n", strerror(errno), errno);
			continue;
		}

		/* skip its error when happened */
		setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

		/* create bind() */
		if (bind(listener, p->ai_addr, p->ai_addrlen) == -1)
		{
			fprintf(stderr, "server: bind socket error: %s(errno: %d), try next\n", strerror(errno), errno);
			close(listener);
			continue;
		}

		break; // scoket created and bind successfully.
	}

	/* if p is NULL, it means bind() is failed */
	if (p == NULL)
	{
		fprintf(stderr, "server: failed to bind\n");
		return 2;
	}

	freeaddrinfo(hostinfo); // all done with this

#ifdef __linux__
	if(KillZombieProcess() != 0)
	{
		perror("server: sigaction fail");
		return 1;
	}
#endif

	/* create listen() */
	if (listen(listener, BACKOG) == -1)
	{
		fprintf(stderr, "listen socket error: %s(errno: %d)\n", strerror(errno), errno);
		return 1;
	}

	return 0;
}

int main(int argc, char* argv[])
{
	/* variable */
	int status;

	fd_set read_fds;	// temporary file descriptor list for select()
	int fdmax;			// max amount of file descriptor

	int newfd;			// new socket descriptor with accept()
	struct sockaddr_storage remoteaddr;		// client address
	socklen_t addrlen;
	char remoteIP[INET6_ADDRSTRLEN];

	char buffer[MAXDATASIZE];		// buffer, store client data buffer
	int nbytes;


	/* setup host, create socket, bind, listen */
	if ( (status =TcpHostSetup()) != 0)
	{
		fprintf(stderr, "server: Initialize failed\n");
		exit(status);
	}

	/* register close server with ctrl+C */
	signal(SIGINT, close_app);

	/* initialize variables */
	sockfd_list.clear();
	FD_ZERO(&read_fds);
	memset(buffer, 0, sizeof(buffer));

	/* set variables */
	sockfd_list.push_back(listener);  // add listener (host socket file descriptor) into master set
	fdmax = listener; // set max number

	fprintf(stdout, "server: waiting for connections...\n");

	/* main loop */
	while (1)
	{
		/* copy master set*/
		AssignFdSetList(&read_fds);

		/* using select() to monitor sockets without timeout */
		if (select(fdmax + 1, &read_fds, NULL, NULL, NULL) == -1) 
		{
			fprintf(stderr, "server: select() failed\n");
			exit(4);
		}

		/* find the data in read_fds */
		for (int list=0; list<(int)sockfd_list.size(); list++)
		{
			int i = sockfd_list[list];
			if (FD_ISSET(i, &read_fds) == true) // find one!!
			{
				if (i == listener)
				{
					/* handle new connections */
					addrlen = sizeof(remoteaddr);
					newfd = accept(listener, (struct sockaddr*)&remoteaddr, &addrlen);
					if(newfd == -1)
						fprintf(stderr, "server: accept() failed - %s(errno: %d)\n", strerror(errno), errno);
					else
					{
						sockfd_list.push_back(newfd); // add this new connection to master
						if (newfd > fdmax)
							fdmax = newfd; // update fdmax
						fprintf(stdout, "server: new connection from %s on socket %d\n",
							inet_ntop(remoteaddr.ss_family, get_in_addr((struct sockaddr*)&remoteaddr), remoteIP, sizeof(remoteIP)),
							newfd);

						/* send hello word message to client */
						memset(buffer, 0, sizeof(buffer));
						sprintf(buffer, "Welcome to connect!, your IP is %s\n", inet_ntop(remoteaddr.ss_family, get_in_addr((struct sockaddr*)&remoteaddr), remoteIP, sizeof(remoteIP)));
						send(newfd, buffer, sizeof(buffer), 0);
					}
				}
				else
				{
					/* handle connections which from client */
					nbytes = recv(i, buffer, sizeof(buffer), 0);
					if (nbytes <= 0)
					{
						/* close the connection */
						if (nbytes == 0)
							fprintf(stdout, "server: socket %d hung up\n", i);
						else
							fprintf(stderr, "server: recv err\n");
						close(i);
						sockfd_list.erase(sockfd_list.begin() + list); // remove it from master
						memset(buffer, 0, sizeof(buffer));
					}
					else
					{
						/* handle client message to other clients */
						fd_set temp;
						AssignFdSetList(&temp);
						for (int j = 0; j <= fdmax; j++)
						{
							if (FD_ISSET(j, &temp) == true)
							{
								if (j != listener && j != i)
								{
									if (send(j, buffer, nbytes, 0) == -1)
									{
										fprintf(stderr, "server: message broad cast failed\n");
									}
									memset(buffer, 0, sizeof(buffer));
								}
							}
						}
					}					
				}// if (i == listener)
			} // if (FD_ISSET(i, &read_fds)
		} // for (int i = 0; i <= fdmax; i++)
	} // while (1)

	return 0;
}
