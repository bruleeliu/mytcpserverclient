#define _WIN32
#ifdef __linux__
#undef _WIN32
#endif

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
using namespace std;

#ifdef _WIN32
#include <WinSock2.h>
#include <Windows.h>
#include <WS2tcpip.h>
#define close(x) closesocket(x)
#endif

#ifdef __linux__
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <wait.h>
#include <sys/time.h>
#endif

#define STDIN 0

void TestFunc1()
{
	struct timeval tv;
	fd_set readfds;
	int status;

	FD_ZERO(&readfds);
	FD_SET(STDIN, &readfds);

	while (1)
	{
		tv.tv_sec = 10;
		tv.tv_usec = 0;


		//fprintf(stdout, "do select() without timeout\n");
		status = select(STDIN + 1, &readfds, NULL, NULL, &tv);
		if (status == -1)
		{
			fprintf(stderr, "select fail\n");
			exit(1);
		}
		else if (status == 0)
		{
			fprintf(stdout, "wait~\n");
			continue;
		}
		else
		{
			if (FD_ISSET(STDIN, &readfds))
			{
				fprintf(stdout, "you press a key\n");
			}
			else
			{
				fprintf(stdout, "select timeout\n");
			}
		}
	}
}

void TestFunc2()
{
	fprintf(stdout, "sizeof:\n");
	fprintf(stdout, "    struct addrinfo = %d\n", sizeof(struct addrinfo));
	fprintf(stdout, "    struct sockaddr = %d\n", sizeof(struct sockaddr));
	fprintf(stdout, "    struct sockaddr_in = %d\n", sizeof(struct sockaddr_in));
	fprintf(stdout, "        struct in_addr = %d\n", sizeof(struct in_addr));
	fprintf(stdout, "    struct sockaddr_in6 = %d\n", sizeof(struct sockaddr_in6));
	fprintf(stdout, "        struct in6_addr = %d\n", sizeof(struct in6_addr));
	fprintf(stdout, "    struct sockaddr_storage = %d\n", sizeof(struct sockaddr_storage));
}

int main(int argc, char* argv[])
{
	TestFunc1();
	//TestFunc2();

	return 0;
}
