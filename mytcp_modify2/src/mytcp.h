#pragma once
#ifndef __MY_TCP_H__
#define __MY_TCP_H__

#include "commonheader.h"

/* for all */
#define HOSTIP "127.0.0.1"
#define HOSTPORT "9044"
#define MAXDATASIZE 1024 // max size for each send
#define MAXMSGLENGTH 1024 // max size for each send
#define MAXUSERNAMELENGTH 25

/* define message pack type */
#define BROADCAST_USRJOIN 0x2E59563A
#define COMMONMESSAGEPACK 0x29DF555B


/* for server */
#define BACKOG 10	// max amount for waiting for connecting queue

/* for client */
#define STDIN 0


struct MsgPackage
{
	const unsigned int header = COMMONMESSAGEPACK;
	//unsigned int totalPackBytes; // without header size
	unsigned int dataBytesInPack; // without header and dataBytesInPack size
	unsigned int nameLen; // actual length
	unsigned int msgLen;  // actual length
	char name[MAXUSERNAMELENGTH];
	char msg[MAXMSGLENGTH];
};

struct UserJoinPackage
{
	const unsigned int header = BROADCAST_USRJOIN;
	//unsigned int totalPackBytes; // without header size
	unsigned int dataBytesInPack; // without header and dataBytesInPack size
	unsigned int msgLen;
	char msg[MAXMSGLENGTH];
};

struct SocketFdInfo
{
	int sockFd;
	char name[MAXUSERNAMELENGTH];
};

class MyBasicTcp
{
public:
	/* function declaration */
	MyBasicTcp();
	~MyBasicTcp();

	int SetupHost(const char* port);                         // for server
	int GetActivitySockfdHost(std::vector<SocketFdInfo>* activeList); // for server
	int SetupClient(const char* ip, const char* port);         // for client
	int GetActivitySockfdClient(std::vector<SocketFdInfo>* activeList); // for client

	int SelectWaitingfor(struct timeval* timeout);
	void Close();

	void CopyMainFdList_NoHost(std::vector<SocketFdInfo>* copyFdList);
	void UpdateMainFdListInfo(SocketFdInfo updateFd);
	void EraseFromMainFdList(int eraseSockFd);
	void AddToMainFdList(int newfd);

	int Send(const char* buf, int len);
	int Receive(char* buf, int len);
	// if sockFd = NULL => use mSockFd itself
	int SendAll(int dstScokFd, char* buf, int* len); 
	int ReceiveAll(int dstScokFd, char* buf, int* len);

	bool IsPackTypeInDefine(unsigned int type);
	unsigned int GetPackType(int dstSockFd);
	int ReceivePack(int dstSockFd, unsigned int type, void* buf, int* len, bool asRawData);
	int SendPack(int dstSockFd, unsigned int type, void* buf, int* len, bool asRawData);

	virtual int MsgWhenServerAccepted(int newSockFd, char* buf, int len);

	//-----------------
#if 0
	int SendPack_Msg(int dstScokFd, MsgPackage* msgPack);    // for client, with pack()
	int ReceivePack_Msg(int dstScokFd, MsgPackage* msgPack); // for client, with unpack()
	int SendPack_RawMsg(int dstScokFd, unsigned char* buff, int* len);    // for server, raw data without pack()/unpack()
	int ReceivePack_RawMsg(int dstScokFd, unsigned char* buff, int* len); // for server, raw data without pack()/unpack()
	int SendBroadcast_UserJoin(int dstSockFd, UserJoinPackage* usrJoinPack);
	int ReceiveBroadcast_UserJoin(int dstSockFd, UserJoinPackage* usrJoinPack);
#endif
	//-----------------


private:
	/* variable declaration */
	int mSockFd;		          // socket descriptor for host listens or client used
	int mFdMax;			          // max amount of file descriptor
	std::vector<SocketFdInfo> mSockFdList; // master file descriptor list
	fd_set mReadFds;	          // temporary file descriptor list for select()

	/* function declaration */
	void* GetInAddr(struct sockaddr* sa); // get IPv4 or IPv6 from sockaddr
	int Accepted(); // for server host
	void ClearMainFdList();
#ifdef _WIN32
	int InitWinSockDll();
	int FreeWinSockDll();
#endif

};

#endif // __MY_TCP_H__