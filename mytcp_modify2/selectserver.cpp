#include "src/mytcp.h"
#include "src/mylib.h"

MyBasicTcp myTcp;


void close_app(int s) {
	/* close server */
	// To Do...
	InfoMsg("\nserver: app is closing....(%d)\n", s);
	myTcp.Close();
	exit(0);
}

int main(int argc, char* argv[])
{
	/* variable */
	int status;
	int nBytes;
	std::vector<SocketFdInfo> activityFdList;
	std::vector<SocketFdInfo> temp;
	unsigned char rawDataBuf[MAXDATASIZE];
	int rawDataBufLen = MAXDATASIZE;
	unsigned int getType = 0;
	UserJoinPackage usrJoinPack;

	DbgMsg(">>Debug Message On<<\n");

	/* setup host, create socket, bind, listen */
	if ((status = myTcp.SetupHost(HOSTPORT)) != 0)
	{
		ErrMsg("server: Initialize failed\n");
		exit(status);
	}

	/* register close server with ctrl+C */
	signal(SIGINT, close_app);


	/* main loop */
	while (1)
	{
		InfoMsg("[server: waiting for connections...]\n");
		if ((status = myTcp.SelectWaitingfor(NULL)) == -1)
		{
			myTcp.Close();
			exit(4);
		}

		activityFdList.clear();

		if (myTcp.GetActivitySockfdHost(&activityFdList) != -1)
		{

			/* there are other activity sockets (no host socket) */
			for (int list = 0; list < (int)activityFdList.size(); list++)
			{
				int i = activityFdList[list].sockFd; // sockfd value

				InfoMsg("server: start to handle socket %d (user name is '%s')\n", i, activityFdList[list].name);

				/* handle connections which from client */
				//nBytes = myTcp.ReceivePack_RawMsg(i, rawDataBuf, &rawDataBufLen);
				getType = myTcp.GetPackType(i);
				if (getType <= 0)
				{
					/* close the connection */
					if (getType == 0)
					{
						InfoMsg("server: socket %d hung up\n", i);

						/* send message to other clinet to notify */
						memset(rawDataBuf, 0, sizeof(rawDataBuf));
						sprintf((char*)rawDataBuf, "'%s' leave the chat room", activityFdList[list].name);
						myTcp.CopyMainFdList_NoHost(&temp);
						for (int listj = 0; listj < temp.size(); listj++)
						{
							if (temp[listj].sockFd == i)
								continue;
							/* broadcast message to each one */
							myTcp.MsgWhenServerAccepted(temp[listj].sockFd, (char*)rawDataBuf, sizeof(rawDataBuf));
						}
					}
					else
						ErrMsg("server: recv err\n");
					close(i);
					myTcp.EraseFromMainFdList(i); // remove it from master
				}
				else // nBytes > 0
				{
					if (myTcp.IsPackTypeInDefine(getType) == false)
						continue;

					/* handle client message to other clients */
					myTcp.CopyMainFdList_NoHost(&temp);
#if 0
					DbgMsg("receive len=%d => ", nBytes);
					for (int m = 0;m < nBytes;m++)
						DbgMsg("%x, ", rawDataBuf[m]);
					DbgMsg("\n");

					DbgMsg("here are %d activity socket need to send\n  each as: ", temp.size());
					for (int m = 0;m < temp.size();m++)
						DbgMsg("%x, ", temp[m].SockFd);
					DbgMsg("\n");
#endif
					if (getType == BROADCAST_USRJOIN)
					{
						nBytes = myTcp.ReceivePack(i, getType, &usrJoinPack, &rawDataBufLen, false);

						SocketFdInfo sInfo;
						sInfo.sockFd = i;
						memset(sInfo.name, 0, MAXUSERNAMELENGTH);
						memcpy(sInfo.name, usrJoinPack.msg, strlen(usrJoinPack.msg));
						myTcp.UpdateMainFdListInfo(sInfo);

						memset(rawDataBuf, 0, sizeof(rawDataBuf));
						sprintf((char*)rawDataBuf, "user \"%s\" joins the chatroom", usrJoinPack.msg);
						usrJoinPack.msgLen = strlen((char*)rawDataBuf);
						memset(usrJoinPack.msg, 0, MAXMSGLENGTH);
						memcpy(usrJoinPack.msg, rawDataBuf, usrJoinPack.msgLen);

						for (int listj = 0; listj < temp.size(); listj++)
						{
							if (temp[listj].sockFd == i)
								continue;
							/* broadcast message to each one */
							nBytes = myTcp.SendPack(temp[listj].sockFd, getType, (void*)&usrJoinPack, &rawDataBufLen, false);
							if (nBytes <= 0)
								ErrMsg("server: BROADCAST_USRJOIN message broad cast failed\n");
						}
					}
					else
					{
						memset(rawDataBuf, 0, sizeof(rawDataBuf));
						nBytes = myTcp.ReceivePack(i, getType, rawDataBuf, &rawDataBufLen, true);

						for (int listj = 0; listj < temp.size(); listj++)
						{
							if (temp[listj].sockFd == i)
								continue;
							/* common message send */
							//if (myTcp.SendPack_RawMsg(temp[listj].sockFd, rawDataBuf, &rawDataBufLen) == -1)
							nBytes = myTcp.SendPack(temp[listj].sockFd, getType, rawDataBuf, &rawDataBufLen, true);
							if (nBytes <= 0)
								ErrMsg("server: other messages broad cast failed\n");
						}
					} // if (getType == BROADCAST_USRJOIN)
				}
			} // for (int list = 0; list < (int)tempfd_set.size(); list++)
		} // if (GetActivitySockfd(&tempfd_set) != -1)

	} // while (1)

	return 0;
}
