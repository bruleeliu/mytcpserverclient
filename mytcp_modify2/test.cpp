#include "src/mytcp.h"
#include "src/mylib.h"

void TestFunc1()
{
	struct timeval tv;
	fd_set readfds;
	int status;

	FD_ZERO(&readfds);
	FD_SET(STDIN, &readfds);

	while (1)
	{
		tv.tv_sec = 10;
		tv.tv_usec = 0;


		//fprintf(stdout, "do select() without timeout\n");
		status = select(STDIN + 1, &readfds, NULL, NULL, &tv);
		if (status == -1)
		{
			fprintf(stderr, "select fail\n");
			exit(1);
		}
		else if (status == 0)
		{
			fprintf(stdout, "wait~\n");
			continue;
		}
		else
		{
			if (FD_ISSET(STDIN, &readfds))
			{
				fprintf(stdout, "you press a key\n");
			}
			else
			{
				fprintf(stdout, "select timeout\n");
			}
		}
	}
}

void TestFunc2()
{
	fprintf(stdout, "sizeof:\n");
	fprintf(stdout, "    struct addrinfo = %lu\n", sizeof(struct addrinfo));
	fprintf(stdout, "    struct sockaddr = %lu\n", sizeof(struct sockaddr));
	fprintf(stdout, "    struct sockaddr_in = %lu\n", sizeof(struct sockaddr_in));
	fprintf(stdout, "        struct in_addr = %lu\n", sizeof(struct in_addr));
	fprintf(stdout, "    struct sockaddr_in6 = %lu\n", sizeof(struct sockaddr_in6));
	fprintf(stdout, "        struct in6_addr = %lu\n", sizeof(struct in6_addr));
	fprintf(stdout, "    struct sockaddr_storage = %lu\n", sizeof(struct sockaddr_storage));
}

void TestFunc3()
{
	//---------
	float f = 3.1415926;
	float f2;
	uint32_t fi;
	fi = pack754_32(f);
	f2 = unpack754_32(fi);
	fprintf(stdout, "f = %.7f\n", f);
	fprintf(stdout, "fi = 0x%08" PRIx32 "\n", fi);
	fprintf(stdout, "f2 = %.7f\n\n", f2);
	//---------
}

void TestFunc4()
{
#ifndef DEBUG

	unsigned char buf[1024];
	unsigned int packetsize;
	unsigned char magic;
	unsigned int ps2;
	int monkeycount;
	long altitude;
	const char* s = "Great unmitigated Zot!  You've found the Runestaff!";
	char s2[96];
	double absurdityfactor;


	//packetsize = pack(buf, "CLlqsd", 'B', 0, 37, (long)-5, s, -3490.5);
	packetsize = pack(buf, "d", -3490.5);
	fprintf(stdout, "packet is %u bytes\n", packetsize);

	fprintf(stdout, "buf is ");
	for (unsigned int i = 0; i < packetsize;i++)
		fprintf(stdout, "'%x', ", buf[i]);
	fprintf(stdout, "\n");

	//unpack(buf, "CLlqsd", &magic, &ps2, &monkeycount, &altitude, s2, &absurdityfactor);
	unpack(buf, "d", &absurdityfactor);

	//fprintf(stdout, "'%c' %hhu %d %ld \"%s\" %f \n", magic, ps2, monkeycount, altitude, s2, absurdityfactor);
	fprintf(stdout, " %f  \n", absurdityfactor);

#else
	unsigned char buf[1024];

	int x;

	long long k, k2;
	long long test64[14] = { 0, -0, 1, 2, -1, -2, 0x7fffffffffffffffll >> 1, 0x7ffffffffffffffell, 0x7fffffffffffffffll, -0x7fffffffffffffffll, -0x8000000000000000ll, 9007199254740991ll, 9007199254740992ll, 9007199254740993ll };

	unsigned long long K, K2;
	unsigned long long testu64[14] = { 0, 0, 1, 2, 0, 0, 0xffffffffffffffffll >> 1, 0xfffffffffffffffell, 0xffffffffffffffffll, 0, 0, 9007199254740991ll, 9007199254740992ll, 9007199254740993ll };

	long i, i2;
	long test32[14] = { 0, -0, 1, 2, -1, -2, 0x7fffffffl >> 1, 0x7ffffffel, 0x7fffffffl, -0x7fffffffl, -0x80000000l, 0, 0, 0 };

	unsigned long I, I2;
	unsigned long testu32[14] = { 0, 0, 1, 2, 0, 0, 0xffffffffl >> 1, 0xfffffffel, 0xffffffffl, 0, 0, 0, 0, 0 };

	int j, j2;
	int test16[14] = { 0, -0, 1, 2, -1, -2, 0x7fff >> 1, 0x7ffe, 0x7fff, -0x7fff, -0x8000, 0, 0, 0 };

	printf("char bytes: %zu\n", sizeof(char));
	printf("int bytes: %zu\n", sizeof(int));
	printf("long bytes: %zu\n", sizeof(long));
	printf("long long bytes: %zu\n", sizeof(long long));
	printf("float bytes: %zu\n", sizeof(float));
	printf("double bytes: %zu\n", sizeof(double));
	printf("long double bytes: %zu\n", sizeof(long double));

	for (x = 0; x < 14; x++) {
		k = test64[x];
		pack(buf, "q", k);
		unpack(buf, "q", &k2);

		if (k2 != k) {
			printf("64: %lld != %lld\n", k, k2);
			printf("  before: %016llx\n", k);
			printf("  after:  %016llx\n", k2);
			printf("  buffer: %02hhx %02hhx %02hhx %02hhx "
				" %02hhx %02hhx %02hhx %02hhx\n",
				buf[0], buf[1], buf[2], buf[3],
				buf[4], buf[5], buf[6], buf[7]);
		}
		else {
			//printf("64: OK: %lld == %lld\n", k, k2);
		}

		K = testu64[x];
		pack(buf, "Q", K);
		unpack(buf, "Q", &K2);

		if (K2 != K) {
			printf("64: %llu != %llu\n", K, K2);
		}
		else {
			//printf("64: OK: %llu == %llu\n", K, K2);
		}

		i = test32[x];
		pack(buf, "l", i);
		unpack(buf, "l", &i2);

		if (i2 != i) {
			printf("32(%d): %ld != %ld\n", x, i, i2);
			printf("  before: %08lx\n", i);
			printf("  after:  %08lx\n", i2);
			printf("  buffer: %02hhx %02hhx %02hhx %02hhx "
				" %02hhx %02hhx %02hhx %02hhx\n",
				buf[0], buf[1], buf[2], buf[3],
				buf[4], buf[5], buf[6], buf[7]);
		}
		else {
			//printf("32: OK: %ld == %ld\n", i, i2);
		}

		I = testu32[x];
		pack(buf, "L", I);
		unpack(buf, "L", &I2);

		if (I2 != I) {
			printf("32(%d): %lu != %lu\n", x, I, I2);
		}
		else {
			//printf("32: OK: %lu == %lu\n", I, I2);
		}

		j = test16[x];
		pack(buf, "h", j);
		unpack(buf, "h", &j2);

		if (j2 != j) {
			printf("16: %d != %d\n", j, j2);
		}
		else {
			//printf("16: OK: %d == %d\n", j, j2);
		}
	}

	if (1) {
		long double testf64[8] = { -3490.6677, 0.0, 1.0, -1.0, DBL_MIN * 2, DBL_MAX / 2, DBL_MIN, DBL_MAX };
		long double f, f2;

		for (i = 0; i < 8; i++) {
			f = testf64[i];
			pack(buf, "g", f);
			unpack(buf, "g", &f2);

			if (f2 != f) {
				printf("f64: %Lf != %Lf\n", f, f2);
				printf("  before: %016llx\n", *((long long*)&f));
				printf("  after:  %016llx\n", *((long long*)&f2));
				printf("  buffer: %02hhx %02hhx %02hhx %02hhx "
					" %02hhx %02hhx %02hhx %02hhx\n",
					buf[0], buf[1], buf[2], buf[3],
					buf[4], buf[5], buf[6], buf[7]);
			}
			else {
				//printf("f64: OK: %f == %f\n", f, f2);
			}
		}
	}
	if (1) {
		double testf32[7] = { 0.0, 1.0, -1.0, 10, -3.6677, 3.1875, -3.1875 };
		double f, f2;

		for (i = 0; i < 7; i++) {
			f = testf32[i];
			pack(buf, "d", f);
			unpack(buf, "d", &f2);

			if (f2 != f) {
				printf("f32: %.10f != %.10f\n", f, f2);
				printf("  before: %016llx\n", *((long long*)&f));
				printf("  after:  %016llx\n", *((long long*)&f2));
				printf("  buffer: %02hhx %02hhx %02hhx %02hhx "
					" %02hhx %02hhx %02hhx %02hhx\n",
					buf[0], buf[1], buf[2], buf[3],
					buf[4], buf[5], buf[6], buf[7]);
			}
			else {
				//printf("f32: OK: %f == %f\n", f, f2);
			}
		}
	}
	if (1) {
		float testf16[7] = { 0.0, 1.0, -1.0, 10, -10, 3.1875, -3.1875 };
		float f, f2;

		for (i = 0; i < 7; i++) {
			f = testf16[i];
			pack(buf, "f", f);
			unpack(buf, "f", &f2);

			if (f2 != f) {
				printf("f16: %f != %f\n", f, f2);
				printf("  before: %08x\n", *((int*)&f));
				printf("  after:  %08x\n", *((int*)&f2));
				printf("  buffer: %02hhx %02hhx %02hhx %02hhx "
					" %02hhx %02hhx %02hhx %02hhx\n",
					buf[0], buf[1], buf[2], buf[3],
					buf[4], buf[5], buf[6], buf[7]);
			}
			else {
				//printf("f16: OK: %f == %f\n", f, f2);
			}
		}
	}
#endif
}

void TestFunc5()
{
	unsigned int uit = 1;
	unsigned int uit2;
	unsigned char buf[MAXDATASIZE];
	unsigned int packetsize;

	fprintf(stdout, "sizeof:\n");
	fprintf(stdout, "  signed char = %lu\n", sizeof(signed char));
	fprintf(stdout, "  unsigned char = %lu\n", sizeof(unsigned char));
	fprintf(stdout, "  int = %lu\n", sizeof(int));
	fprintf(stdout, "  unsigned int = %lu\n", sizeof(unsigned int));
	fprintf(stdout, "  long int = %lu\n", sizeof(long int));
	fprintf(stdout, "  unsigned long int = %lu\n", sizeof(unsigned long int));
	fprintf(stdout, "  long long int = %lu\n", sizeof(long long int));
	fprintf(stdout, "  unsigned long long int = %lu\n", sizeof(unsigned long long int));
	fprintf(stdout, "  float = %lu\n", sizeof(float));
	fprintf(stdout, "  double = %lu\n", sizeof(double));
	fprintf(stdout, "  long double = %lu\n", sizeof(long double));
	fprintf(stdout, "  short = %lu\n", sizeof(short));
	fprintf(stdout, "  unsigned short = %lu\n", sizeof(unsigned short));
	fprintf(stdout, "  long long = %lu\n", sizeof(long long));
	fprintf(stdout, "  short int = %lu\n", sizeof(short int));
	fprintf(stdout, "  unsigned short int = %lu\n", sizeof(unsigned short int));
	fprintf(stdout, "  long = %lu\n", sizeof(long));
	fprintf(stdout, "'\n");

	fprintf(stdout, "packet => %hu (sizeof is %lu)\n", uit, sizeof(uit));
	memset(buf, 0, sizeof(buf));
	packetsize = pack(buf, "L", uit);
	fprintf(stdout, "  packet is %u bytes\n", packetsize);
	fprintf(stdout, "  buf is '");
	for (unsigned int i = 0; i < packetsize;i++)
		fprintf(stdout, "%d, ", buf[i]);
	fprintf(stdout, "'\n");

	//memset(buf, 0, sizeof(buf));
	unpack(buf, "L", &uit2);
	fprintf(stdout, "unpacket => %hu\n", uit2);
	fprintf(stdout, "\n");
}

int main(int argc, char* argv[])
{
	if (argc == 2)
	{
		switch (atoi(argv[1]))
		{
			case 1:
				TestFunc1();
				break;
			case 2:
				TestFunc2();
				break;
			case 3:
				TestFunc3();
				break;
			case 4:
				TestFunc4();
				break;
			case 5:
				TestFunc5();
				break;
			default:
				break;
		}
	}

	return 0;
}
