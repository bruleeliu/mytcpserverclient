#include "src/mytcp.h"
#include "src/mylib.h"

MyBasicTcp myTcp;


void close_app(int s) {
	/* close app */
	// To Do...
	InfoMsg("\nclient: app is closing....(%d)\n", s);
	myTcp.Close();
	exit(0);
}

void AssignUserName(char* inName, char* instr, int* len)
{
	*len = strlen(inName);// +1;
	if (*len >= MAXUSERNAMELENGTH)
		*len = MAXUSERNAMELENGTH;
	memset(instr, 0, MAXUSERNAMELENGTH);
	memcpy(instr, inName, *len/* - 1*/);
	//outMsgPack->name[outMsgPack->nameLen - 1] = '\0';
}

int main(int argc, char* argv[])
{
	/* variable */
	int status;
	struct timeval tv;
	std::vector<SocketFdInfo> activityFdList;
	MsgPackage msgPack;
	UserJoinPackage usrJoinPack;
	int value = 0;


	DbgMsg(">>Debug Message On<<\n");

	/* check input */
	if (argc != 2)
	{
		ErrMsg("Usage: %s username\n", argv[0]);
		exit(1);
	}

	/* setup client, create socket, connect.. */
	if ((status = myTcp.SetupClient(HOSTIP, HOSTPORT)) != 0)
	{
		ErrMsg("client: initialize failed\n");
		exit(status);
	}

	/* send user name to server to broadcast to everyone known */
	AssignUserName(argv[1], usrJoinPack.msg, (int*)&usrJoinPack.msgLen);
	myTcp.SendPack(NULL, BROADCAST_USRJOIN, (void*)&usrJoinPack, &value, false); // send clinet name to server

	/* register close server with ctrl+C */
	signal(SIGINT, close_app);

	/* set variables */
	myTcp.AddToMainFdList(STDIN);

	/* main loop */
	while (1)
	{
		tv.tv_sec = 60;
		tv.tv_usec = 0;

		status = myTcp.SelectWaitingfor(&tv);
		if (status == -1)
		{
			ErrMsg("client: select() failed\n");
			myTcp.Close();
			exit(4);
		}
		else if (status == 0)
		{
			InfoMsg("[No any messages, waiting...]\n");
			continue;
		}
		else
		{
			/* handle message */
			activityFdList.clear();
			if (myTcp.GetActivitySockfdClient(&activityFdList) != -1)
			{
				for (int list = 0; list < (int)activityFdList.size(); list++)
				{
					int sockFd = activityFdList[list].sockFd; // sockfd value

					if (sockFd == STDIN)
					{
						fgets(msgPack.msg, sizeof(msgPack.msg), stdin);
						AssignUserName(argv[1], msgPack.name, (int*)&msgPack.nameLen);
						msgPack.msg[strlen(msgPack.msg) - 1] = '\0';  // replace last char from '\n' to '\0'
						msgPack.msgLen = strlen(msgPack.msg);
						//myTcp.SendPack_Msg(NULL, &msgPack);
						myTcp.SendPack(NULL, COMMONMESSAGEPACK, &msgPack, &value, false);
						InfoMsg("(%s):<= %s\n", msgPack.name, msgPack.msg);
#if 0
						{
							unsigned char buf[MAXDATASIZE];
							unsigned int packResultSize;
							packResultSize =
								sizeof(msgPack.totalPackBytes) +
								sizeof(msgPack.nameLen) +
								sizeof(msgPack.msgLen) +
								msgPack.nameLen +
								msgPack.msgLen;
							msgPack.totalPackBytes = packResultSize;
							DbgMsg("--------\n");
							DbgMsg("msgPack:\n");
							DbgMsg("  totalPackBytes = %d (%x)\n", msgPack.totalPackBytes, msgPack.totalPackBytes);
							DbgMsg("  nameLen = %d (%x)\n", msgPack.nameLen, msgPack.nameLen);
							DbgMsg("  msgLen = %d (%x)\n", msgPack.msgLen, msgPack.msgLen);
							DbgMsg("  name = %s\n", msgPack.name);
							DbgMsg("  msg = %s\n", msgPack.msg);
							pack(buf, "LLLss", 
								msgPack.totalPackBytes,
								msgPack.nameLen,
								msgPack.msgLen,
								msgPack.name,
								msgPack.msg
								);
							DbgMsg("packBuff: ");
							for (int i = 0;i < packResultSize;i++)
								DbgMsg("%x, ", buf[i]);
							DbgMsg("\n");

							MsgPackage mp;
							unpack(buf, "LLLss",
								&mp.totalPackBytes,
								&mp.nameLen,
								&mp.msgLen,
								mp.name,
								mp.msg
							);
							DbgMsg("unpack msgPack:\n");
							DbgMsg("  totalPackBytes = %d (%x)\n", mp.totalPackBytes, mp.totalPackBytes);
							DbgMsg("  nameLen = %d (%x)\n", mp.nameLen, mp.nameLen);
							DbgMsg("  msgLen = %d (%x)\n", mp.msgLen, mp.msgLen);
							DbgMsg("  name = %s\n", mp.name);
							DbgMsg("  msg = %s\n", mp.msg);
							DbgMsg("--------\n");
						}
#endif
					}
					else
					{
						//status = myTcp.ReceivePack_Msg(NULL, &msgPack); // status is receive bytes, here
						value = myTcp.GetPackType(NULL);
						if (value == 0)
						{
							/* server close...close client */
							ErrMsg("[server is shutdown...]\n");
							myTcp.Close();
							exit(0);
						}
						else if (value > 0)
						{
							if (myTcp.IsPackTypeInDefine(value) == false)
								continue;
							if (value == BROADCAST_USRJOIN)
							{
								/* receive broadcast message */
								myTcp.ReceivePack(NULL, BROADCAST_USRJOIN, (void*)&usrJoinPack, &value, false);
								InfoMsg("[Broadcast Message]: %s\n", usrJoinPack.msg);
							}
							if (value == COMMONMESSAGEPACK)
							{
								/* receive common message */
								myTcp.ReceivePack(NULL, COMMONMESSAGEPACK, (void*)&msgPack, &value, false);
								InfoMsg("(%s):=> %s\n", msgPack.name, msgPack.msg);
							}
						}
					}
				} // for (....
			}

		} // if (status == -1)
	}// while (1)


	return 0;
}