#define _WIN32
#ifdef __linux__
#undef _WIN32
#endif

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
using namespace std;

#ifdef _WIN32
#include <WinSock2.h>
#include <Windows.h>
#include <WS2tcpip.h>
#define close(x) closesocket(x)
#endif

#ifdef __linux__
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <wait.h>
#endif


#define MYPORT "3698"
#define BACKOG 10 // max amount for waiting for connecting queue
int sockfd; // server socket

void close_app(int s) {
	/* close server */
	// To Do...
	fprintf(stdout, "\nserver: app is closing....\n");
	close(sockfd);
	exit(0);
}

#ifdef __linux__
void sigchld_handler(int s)
{
	while (waitpid(-1, NULL, WNOHANG) > 0);
}
#endif

/* get IPv4 or IPv6 from sockaddr */
void* get_in_addr(struct sockaddr* sa)
{
	if (sa->sa_family == AF_INET) 
	{
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}
	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int TcpHostSetup()
{
	/* variable */
	int status;
	struct addrinfo hints;
	struct addrinfo* hostinfo;
	struct addrinfo* p; // point for for-loop
	int yes = 1;
#ifdef __linux__
	struct sigaction sa; // for linux kill zombie process
#endif

	/* get hostinfo */
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC; // IPv4 or IPv6
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; // auto IP from kernel
	status = getaddrinfo(NULL, MYPORT, &hints, &hostinfo);
	if (status != 0)
	{
		fprintf(stderr, "server: getaddrinfo error - %s\n", gai_strerror(status));
		return 1;
	}

	/* create socket() at first could be bind in hostinfo */
	for (p = hostinfo; p != NULL; p = p->ai_next)
	{
		sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
		if (sockfd == -1)
		{
			//perror("server: socket fail and try next");
			fprintf(stderr, "server: create socket error: %s(errno: %d), try next\n", strerror(errno), errno);
			continue;
		}

		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
		{
			perror("server: setsockopt error");
			return 1;
		}

		/* create bind() */
		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1)
		{
			//perror("server: bind");
			fprintf(stderr, "server: bind socket error: %s(errno: %d), try next\n", strerror(errno), errno);
			close(sockfd);
			continue;
		}
		break; // create socket pass and bind pass
	}

	if (p == NULL)
	{
		fprintf(stderr, "server: failed to bind\n");
		return 2;
	}
	freeaddrinfo(hostinfo); // free hostinfo here (not be used below)

	/* create listen() */
	if (listen(sockfd, BACKOG) == -1)
	{
		fprintf(stderr, "listen socket error: %s(errno: %d)\n", strerror(errno), errno);
		return 1;
	}

#ifdef __linux__
	/* clear all linux zombie process*/
	sa.sa_handler = sigchld_handler; // kill all zombie process
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;

	if (sigaction(SIGCHLD, &sa, NULL) == -1)
	{
		perror("server: sigaction fail");
		return 1;
	}
#endif

	fprintf(stdout, "server: waiting for connections...\n");
	return 0;
}

int main(int argc, char* argv[])
{
	/* variable */
	int status;
	struct sockaddr their_addr;
	socklen_t addr_size;
	int new_sockfd; // client connect
	char clientIP[INET6_ADDRSTRLEN];
	char* srv_msg = (char*)"Hello! You connected to server!";
	char recv_buff[1024];
	int bytes_read;


	/* check input */
	//if (argc >= 2)
	//{
	//	cerr << "Usage: ./server port ..." << endl;
	//	return 0;
	//}
	signal(SIGINT, close_app); // register close server with ctrl+C

	/* setup host, create socket, bind, listen ... */
	if ( (status =TcpHostSetup()) != 0)
	{
		fprintf(stderr, "server: Initialize failed\n");
		exit(status);
	}



	/* main loop for accept(), send(), recv() */
	while (1)
	{
		/* create accept */
		addr_size = sizeof(their_addr);
		new_sockfd = accept(sockfd, (struct sockaddr*)&their_addr, &addr_size);
		if (new_sockfd == -1)
		{
			fprintf(stderr, "create client socket error: %s(errno: %d)\n", strerror(errno), errno);
			//exit(1);
			continue;
		}

		/* list IP who connected */
		inet_ntop(their_addr.sa_family, get_in_addr((struct sockaddr*)&their_addr), clientIP, sizeof(clientIP));
		fprintf(stdout, "server: got connection from %s\n", clientIP);

		/* recv or send */
		if (!fork())
		{
			/* this is child process */

			close(sockfd); // close host socket, because it doesn't need

			/* send hello message to client*/
			if (send(new_sockfd, srv_msg, sizeof(srv_msg), 0) == -1)
			{
				fprintf(stderr, "server->client: send hello message failed : %s(errno: %d)\n", strerror(errno), errno);
				//exit(1);
			}

			/* receive message */
			memset(recv_buff, 0, sizeof(recv_buff));
			bytes_read = recv(new_sockfd, recv_buff, sizeof(recv_buff), 0);
			if (bytes_read > 0)
			{
				fprintf(stdout, "server<-client: receive message (%d bytes) => %s\n", bytes_read, recv_buff);

				/* send back same message to client*/
				if (send(new_sockfd, recv_buff, sizeof(bytes_read), 0) == -1)
				{
					fprintf(stderr, "server->client: reply message failed\n");
				}
			}
			else if (bytes_read == -1)
			{
				fprintf(stderr, "server<-client: receive something wrong (%d)\n", bytes_read);
			}

			close(new_sockfd);
			exit(0);
		}

	}

	/* close socket */
	//close(new_sockfd);
	close(sockfd);

	return 0;
}